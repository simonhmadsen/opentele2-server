package org.opentele.server.provider.Exception

class InvalidSmsIntegrationConfigurationException extends RuntimeException {
    InvalidSmsIntegrationConfigurationException() {
    }

    InvalidSmsIntegrationConfigurationException(String s) {
        super(s)
    }

    InvalidSmsIntegrationConfigurationException(String s, Throwable throwable) {
        super(s, throwable)
    }

    InvalidSmsIntegrationConfigurationException(Throwable throwable) {
        super(throwable)
    }

    InvalidSmsIntegrationConfigurationException(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1)
    }
}
