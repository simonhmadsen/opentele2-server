package org.opentele.taglib

import grails.test.mixin.TestFor
import spock.lang.Specification
import spock.lang.Unroll

@TestFor(ThresholdTagLib)
class ThresholdTagLibSpec extends Specification {

    @Unroll
    def "test that 0 thresholds are shown correctly according to locale()"() {

        when:
        def attrs = [field: null, level: "alertHigh", threshold: [name: "CRP", alertHigh: threshold ]]

        Locale.setDefault(Locale.forLanguageTag(languageTag))

        then:
        tagLib.formatThreshold(attrs) == expected

        where:
        threshold  | languageTag | expected
        0.12f      | 'da-DK'     | "0,1"
        0.0f       | 'da-DK'     | "0,0"
        0.12f      | 'en-US'     | "0.1"
        0.0f       | 'en-US'     | "0.0"
        null       | 'da-DK'     | "-"
    }

    @Unroll
    def "test that long numbers are shown correctly according to locale()"() {

        when:
        def attrs = [field: null, level: "alertHigh", threshold: [name: "CRP", alertHigh: threshold ]]

        Locale.setDefault(Locale.forLanguageTag(languageTag))

        then:
        tagLib.formatThreshold(attrs) == expected

        where:
        threshold   | languageTag | expected
        1000.12f    | 'da-DK'     | "1000,1"
        1000.12f    | 'en-US'     | "1000.1"
        1000000.12f | 'en-US'     | "1000000.1"
    }


}