package org.opentele.server.provider

import grails.buildtestdata.mixin.Build
import grails.test.mixin.TestFor
import org.opentele.server.model.Patient
import org.opentele.server.model.TextMessageTemplate
import org.opentele.server.provider.Exception.FailedToSendSmsException
import org.opentele.server.provider.Exception.InvalidSmsIntegrationConfigurationException
import org.opentele.server.provider.integration.sms.TextMessagesService
import spock.lang.Specification

@TestFor(TextMessagesController)
@Build([TextMessageTemplate, Patient])
class TextMessagesControllerSpec extends Specification{

    def textMessagesService
    def sessionService

    def setup() {
        textMessagesService = Mock(TextMessagesService)
        controller.textMessagesService = textMessagesService

        sessionService = Mock(SessionService)
        controller.sessionService = sessionService
    }

    def "can get list of templates for send view"() {
        given:
        3.times { TextMessageTemplate.build().save(failOnError: true) }

        when:
        params.id = "42"
        def viewModel = controller.send()

        then:
        viewModel.templates.size() == 3
        viewModel.templates.each { TextMessageTemplate.get(it.id) != null }
        viewModel.patientId == "42"
    }

    def "send view gets empty list of no templates are defined"() {
        when:
        params.id = "42"
        def viewModel = controller.send()

        then:
        viewModel.templates.size() == 0
        viewModel.patientId == "42"
    }

    def "if trying to show send view without patient id user is redirected to patient overview"() {
        when:
        controller.send()

        then:
        response.redirectedUrl == "/patientOverview/index"
        1 * sessionService.setNoPatient(_)
    }

    def "text message can be send to patient"() {
        given:
        def patient = Patient.build(mobilePhone: "12345678").save(failOnError: true)

        when:
        params.selected = "send me now"
        params.patientId = patient.id.toString()
        controller.submit()

        then:
        1 * textMessagesService.send("12345678", "send me now")
        flash.message.contains("message.sent")
        response.redirectedUrl == "/textMessages/send/${patient.id}"
    }

    def "when text message cannot be send message is shown to user"() {
        given:
        def patient = Patient.build(mobilePhone: "12345678").save(failOnError: true)
        textMessagesService.send(_, _) >> { no, text -> throw new FailedToSendSmsException() }

        when:
        params.selected = "send me now"
        params.patientId = patient.id.toString()
        controller.submit()

        then:
        flash.message.contains("message.sent.failed")
        response.redirectedUrl == "/textMessages/send/${patient.id}"
    }

    def "when sms integration is not configured message is shown to user"() {
        given:
        def patient = Patient.build(mobilePhone: "12345678").save(failOnError: true)
        textMessagesService.send(_, _) >> { no, text -> throw new InvalidSmsIntegrationConfigurationException() }

        when:
        params.selected = "send me now"
        params.patientId = patient.id.toString()
        controller.submit()

        then:
        flash.message.contains("textMessages.sms.notconfigured")
        response.redirectedUrl == "/textMessages/send/${patient.id}"
    }

    def "if trying to send text message without patient id user is redirected to patient overview"() {
        when:
        params.selected = "send me"
        controller.submit()

        then:
        response.redirectedUrl == "/patientOverview/index"
        1 * sessionService.setNoPatient(_)
    }

    def "if trying to send text message without message content exception is thrown"() {
        given:
        def patient = Patient.build().save(failOnError: true)

        when:
        params.patientId = patient.id.toString()
        controller.submit()

        then:
        def ex = thrown(IllegalArgumentException)
        ex.message.contains("selected")
    }

    def "if patient not found when trying to send text message message is shown and user is redirected"() {
        when:
        params.selected = "send me now"
        params.patientId = "42"
        controller.submit()

        then:
        response.redirectedUrl == "/patientOverview/index"
        1 * sessionService.setNoPatient(_)
    }
}
