package org.opentele.server.provider

import grails.buildtestdata.mixin.Build
import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import grails.test.mixin.TestMixin
import org.opentele.server.core.exception.EntityNotFoundException
import org.opentele.server.model.Link
import org.opentele.server.model.LinksCategory
import org.opentele.server.model.PatientGroup
import org.opentele.server.model.TextMessageTemplate
import spock.lang.Specification
import grails.test.mixin.gorm.Domain
import grails.test.mixin.hibernate.HibernateTestMixin

@TestFor(LinksService)
@Mock([LinksCategory, Link, PatientGroup])
@Build([PatientGroup])
class LinksServiceSpec extends Specification {

    PatientGroup patientGroup1
    PatientGroup patientGroup2

    def setup() {
        patientGroup1 = PatientGroup.build().save(failOnError: true)
        patientGroup2 = PatientGroup.build().save(failOnError: true)
    }

    def "can create and save valid links category"() {
        given:
        List links = [[title: 'new link1', url: 'http://some.valid.com'], [title: 'new link2', url: 'http://www.url.com']]
        List<Long> patientGroupIds = [patientGroup1.id, patientGroup2.id]

        when:
        def saved = service.createCategory('new category', links, patientGroupIds)

        then:
        saved.hasErrors() == false
        LinksCategory.get(saved.id) != null
    }

    def "validation errors are returned"() {
        given:
        List<Long> patientGroupIds = [patientGroup1.id, patientGroup2.id]

        when:
        def notSaved = service.createCategory('name', [], patientGroupIds)

        then:
        notSaved.hasErrors() == true
        notSaved.id == null
    }

    def "if trying to update non-existing links category exception is thrown"() {
        when:
        service.updateCategory(42, 1, "bla", [], [])

        then:
        thrown(EntityNotFoundException)
    }

    def "if trying to update existing links category with version mismatch update fails"() {
        given:
        def toUpdate = createLinksCategory()
        toUpdate.name = "force version number to be upped"
        toUpdate.save(flush: true)

        when:
        def updated = service.updateCategory(toUpdate.id, 0, "updated name", [], [])

        then:
        updated.hasErrors() == true
        updated.name == "force version number to be upped"
    }

    def "if trying to delete non-existing links category exception is thrown"() {
        when:
        service.deleteCategory(42)

        then:
        thrown(EntityNotFoundException)
    }

    private def createLinksCategory() {
        def category = new LinksCategory(name: "name")
        category.addToLinks(new Link(title: 'link title', url: 'http://www.bt.dk'))
        category.addToPatientGroups(patientGroup1)

        return category.save(failOnError: true, flush: true)
    }
}
