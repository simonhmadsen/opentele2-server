package org.opentele.server.provider
import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import grails.validation.ValidationException
import org.opentele.server.core.command.NumericThresholdCommand
import org.opentele.server.core.model.types.MeasurementTypeName
import org.opentele.server.model.NumericThreshold
import org.opentele.server.model.Threshold
import spock.lang.Specification

@TestFor(ThresholdService)
@Mock([Threshold, NumericThreshold])
class ThresholdServiceSpec extends Specification{

    def "should skip updating and return domain object with errors when validation fails"() {
        given:
        float illegalValue = -1
        def command = new NumericThresholdCommand(alertHigh: 4, warningHigh: 3, warningLow: 2, alertLow: illegalValue,
                                                    threshold: new NumericThreshold(type: MeasurementTypeName.HEMOGLOBIN))

        when:
        service.updateThreshold(command)

        then:
        def ex = thrown ValidationException
        ex.errors.errorCount == 1
        ex.errors.fieldError.field == 'alertLow'
    }

    def "should update threshold and return valid domain when validation succeeds"() {
        given:
        def command = new NumericThresholdCommand(alertHigh: 4, warningHigh: 3, warningLow: 2, alertLow: 1,
                                                    threshold: new NumericThreshold(type: MeasurementTypeName.HEMOGLOBIN))

        when:
        service.updateThreshold(command)

        then:
        !command.hasErrors()
    }
}
