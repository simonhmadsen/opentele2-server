package org.opentele.server.provider

import grails.buildtestdata.mixin.Build
import grails.converters.JSON
import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import org.opentele.server.core.exception.EntityNotFoundException
import org.opentele.server.model.Link
import org.opentele.server.model.LinksCategory
import org.opentele.server.model.PatientGroup
import spock.lang.Specification

@TestFor(LinksController)
@Build([PatientGroup, LinksCategory, Link])
@Mock([PatientGroup, LinksCategory, Link])
class LinksControllerSpec extends Specification {

    def mockLinksService

    def setup() {
        mockLinksService = Mock(LinksService)
        controller.linksService = mockLinksService
    }

    def "index action is redirected to list action"() {
        when:
        controller.index()

        then:
        response.redirectedUrl == "/links/list"
    }

    def "can generate view model for list categories"() {
        given:
        def group1 = PatientGroup.build(name: 'group1').save(failOnError: true)
        def group2 = PatientGroup.build(name: 'group2').save(failOnError: true)
        def c1 = new LinksCategory(name: 'links category 1')
        c1.addToPatientGroups(group1)
        c1.addToPatientGroups(group2)
        c1.addToLinks(new Link(title: 'bla', url: 'http://www.jp.dk'))
        c1.save(failOnError: true)
        def c2 = new LinksCategory(name: 'links category 2')
        c2.addToLinks(new Link(title: 'bla', url: 'http://www.jp.dk'))
        c2.addToPatientGroups(group1)
        c2.save(failOnError: true)

        when:
        def viewModel = controller.list()

        then:
        viewModel.categories[0].name == "links category 1"
        viewModel.categories[0].patientGroupNames == "group1, group2"
        viewModel.categories[1].name == "links category 2"
        viewModel.categories[1].patientGroupNames == "group1"
        viewModel.categoriesTotal == 2
    }

    def "can show first page of categories"() {
        given:
        createCategories(12)

        when:
        def viewModel = controller.list()

        then:
        viewModel.categories.size() == 10
        viewModel.categoriesTotal == 12
        viewModel.categories.first().name == "category_1"
        viewModel.categories.last().name == "category_10"
    }

    def "can show next page of categories"() {
        given:
        createCategories(12)

        when:
        params.offset = 10
        def viewModel = controller.list()

        then:
        viewModel.categories.size() == 2
        viewModel.categoriesTotal == 12
        viewModel.categories.first().name == "category_11"
        viewModel.categories.last().name == "category_12"
    }

    def "can request to create new links category"() {
        given:
        PatientGroup.build().save()

        when:
        def viewModel = controller.create()

        then:
        viewModel.category.class == LinksCategory
        viewModel.currentLinks != null
        viewModel.currentPatientGroupIds != null
        viewModel.allPatientGroups != null
    }

    def "can save new links category with one patient group and one link"() {
        given:
        params.name = 'new category'
        params.selected = '1'
        params.links = '[{title: "new link", url: "http://www.jp.dk"}]'

        when:
        controller.save()

        then:
        1 * mockLinksService.createCategory('new category', [[title: 'new link', url: 'http://www.jp.dk']], [1]) >> new LinksCategory()
        flash.message.contains("created")
        response.redirectedUrl == "/links/list"
    }

    def "can save new links category with two patient groups and two links"() {
        given:
        params.name = 'new category'
        params.selected = ['1', 2]
        params.links = '[{title: "new link", url: "http://www.jp.dk"}, {title: "new link2", url: "http://www.bt.dk"}]'

        when:
        controller.save()

        then:
        1 * mockLinksService.createCategory('new category',
                                            [[title: 'new link', url: 'http://www.jp.dk'], [title: 'new link2', url: 'http://www.bt.dk']],
                                            [1, 2]) >> new LinksCategory()
        flash.message.contains("created")
        response.redirectedUrl == "/links/list"
    }

    def "when trying to save new category create view is re-rendered if validation fails during save"() {
        given:
        def invalidCategory = new LinksCategory()
        invalidCategory.validate()
        mockLinksService.createCategory(_, _, _) >> invalidCategory

        when:
        controller.save()

        then:
        view == "/links/create"
        model.category == invalidCategory
    }

    def "can show specific category"() {
        given:
        createCategories(3)

        when:
        params.id = LinksCategory.list().last().id.toString()
        def viewModel = controller.show()

        then:
        viewModel.category != null
        viewModel.category.id == params.long('id')
    }

    def "show redirects to list action and shows error message if specific category is not found"() {
        when:
        params.id = "42"
        controller.show()

        then:
        response.redirectedUrl == "/links/list"
        flash.message.contains("not.found")
    }

    def "can request to edit existing category"() {
        given:
        createCategories(3)
        def toEdit = LinksCategory.list().last()

        when:
        params.id = toEdit.id.toString()
        def viewModel = controller.edit()

        then:
        viewModel.category == toEdit
        viewModel.currentLinks == (String) (toEdit.links as JSON)
        viewModel.allPatientGroups[0] == PatientGroup.list()[0]
        viewModel.currentPatientGroupIds == toEdit.patientGroups.collect {it.id}
    }

    def "when trying to edit non-existing template user is redirected to list and message is shown"() {
        when:
        params.id = "42"
        controller.edit()

        then:
        response.redirectedUrl == "/links/list"
        flash.message.contains("not.found")
    }

    def "can update existing category"() {
        given:
        createCategories(2)
        def toUpdate = LinksCategory.list().first()

        when:
        params.id = toUpdate.id.toString()
        params.version = toUpdate.version.toString()
        params.name = "updated name"
        params.selected = ["1", "2"] // patient group ids
        params.links = "[{title: 'link title', url: 'some url'}]"
        controller.update()

        then:
        1 * mockLinksService.updateCategory(toUpdate.id, toUpdate.version, "updated name", [[title: 'link title', url: 'some url']], [1, 2]) >> toUpdate
        response.redirectedUrl == "/links/show/${toUpdate.id}"
        flash.message.contains("updated")
    }

    def "when trying to update category edit view is re-rendered if validation fails during save" () {
        given:
        def invalidCategory = new LinksCategory(links: [], patientGroups: [])
        invalidCategory.validate()
        mockLinksService.updateCategory(_, _, _, _, _) >> invalidCategory

        when:
        params.id = "42"
        params.version = "1"
        controller.update()

        then:
        view == "/links/edit"
        model.category == invalidCategory
    }

    def "if category has been deleted by another user while editing user is redirected to list and message shown"() {
        given:
        mockLinksService.updateCategory(_, _, _, _, _) >> {throw new EntityNotFoundException()}

        when:
        params.id = "42"
        params.version = "1"
        controller.update()

        then:
        response.redirectedUrl == "/links/list"
        flash.message.contains("deleted.by.other")
    }

    def "can delete category"() {
        given:
        createCategories(2)
        def toDelete = LinksCategory.findAll().first()

        when:
        params.id = toDelete.id.toString()
        controller.delete()

        then:
        1 * mockLinksService.deleteCategory(toDelete.id)
        response.redirectedUrl == "/links/list"
        flash.message.contains("default.deleted.message")
    }

    def "if trying to delete category already deleted user is redirected to list and message shown"() {
        given:
        mockLinksService.deleteCategory(_) >> { throw new EntityNotFoundException()}

        when:
        params.id = "42"
        controller.delete()

        then:
        response.redirectedUrl == "/links/list"
        flash.message.contains("not.found")
    }

    private createCategories(int noOfCategories) {
        def group = PatientGroup.build().save(failOnError: true)
        noOfCategories.times {idx ->
            def category = new LinksCategory(name: "category_${idx + 1}")
            category.addToPatientGroups(group)
            category.addToLinks(new Link(title: "bla ${idx + 1}", url: "http://www.jp.dk"))
            category.save(failOnError: true)
        }
    }
}
