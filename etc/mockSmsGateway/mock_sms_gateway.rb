require 'sinatra'
require 'json'

set :environment, :production
set :port, 5678

get '/smsgateway/api/sms/send' do
  phoneNo = params['number']
  text = params['text']

  puts "================================================================================================================="
  puts "SMS send request to phone number '#{phoneNo}' with message text '#{text}' received."
  puts "================================================================================================================="

  content_type :json
  { :succes => 1 }.to_json

end