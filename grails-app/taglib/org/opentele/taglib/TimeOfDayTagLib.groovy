package org.opentele.taglib

import org.opentele.server.core.model.Schedule

class TimeOfDayTagLib {
    static namespace = "tod"

    def dateTimeService

    def timeOfDay = { attrs, body ->
        String name = attrs.name
        String id = attrs.id ?: name
        Schedule.TimeOfDay value = attrs.value
        String amPm = 'AM'

        def uses12HourClock = dateTimeService.uses12HourClock(request)
        Schedule.TimeOfDay originalValue = Schedule.TimeOfDay.toTimeOfDay(value.hour, value.minute)

        if (uses12HourClock && value.hour >= 12) {
            amPm = 'PM'
        }
        if (uses12HourClock && value.hour > 12) {
            value.hour = value.hour - 12
        }

        def model = [
                name : name,
                id: id,
                value: value,
                originalValue: originalValue,
                is12HourClock: uses12HourClock,
                maxHour: uses12HourClock ? 12 : 23,
                minHour: uses12HourClock ? 1 : 0,
                amPm: amPm,
                body: body
        ]

        out << g.render(template: "/schedule/timeOfDay", model: model)
    }
}
