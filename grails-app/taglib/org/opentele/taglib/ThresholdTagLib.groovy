package org.opentele.taglib

import org.opentele.server.core.util.NumberFormatUtil
import org.springframework.web.servlet.support.RequestContextUtils

class ThresholdTagLib {

    def formatThreshold = { attrs, body ->

        String propertyName = getPropertyName(attrs.field, attrs.level)

        def value = attrs.threshold."${propertyName}"

        if (value != null && value != "") {

            def locale = RequestContextUtils.getLocale(request)
            if (value.getClass() == Float) {
                value = NumberFormatUtil.formatFloatOneDecimal(value)
            }
            out << value
        } else {
            out << '-'
        }
    }

    private String getPropertyName(String field, String level) {
        if (field) {
            return field + level
        } else {
            return level[0].toLowerCase() + level.substring(1)
        }
    }
}
