<!doctype html>
<html>
<head>
<title><g:message code="production.error.title" /></title>
<meta name="layout" content="main">
<link rel="stylesheet"
	href="${resource(dir: 'css', file: 'errors.css')}" type="text/css">
<style>
    #leftmenu {
        visibility: hidden;
    }
    h2 {
        margin-left: 0px;
    }
</style>
</head>
<body>
    <h2><g:message code="production.error.heading" args="${[formatDate(type: 'both', style: 'SHORT', date: new java.util.Date())]}" /></h2>
    <p><g:message code="production.error.line1" /></p>
    <p><g:message code="production.error.line2" /></p>
</body>
</html>