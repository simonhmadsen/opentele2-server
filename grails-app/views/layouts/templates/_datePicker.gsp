<span class="ui-datepicker-opentele" id="${name}_datepicker">
    <div class="ui-datepicker-wrapper"><g:textField name="${name}_widget" id="${id}_widget" value="${dateAsText}" class="ui-datepicker-ot-date"/></div>
    <g:if test="${showTime}">
        <div class="ui-datepicker-timewrapper">
            <g:message code="default.time.name"/>
            <g:textField name="${name}_input_hour" value="${formatDate(date: date, format: "hh")}" class="ui-datepicker-ot-time" maxlength="2"/>
            <g:message code="default.time.separator"/>
            <g:textField name="${name}_minute" value="${formatDate(date: date, format: "mm")}" class="ui-datepicker-ot-time" maxlength="2"/>
            <g:if test="${is12HourClock}">
                <g:select name="${name}_ampm" from="${['AM', 'PM']}" value="${formatDate(date: date, format: "aa")}" class="ui-datepicker-ot-time"/>
            </g:if>
        </div>
    </g:if>
    <g:hiddenField name="${name}" value="date.struct"/>
    <g:hiddenField name="${name}_day" value="${formatDate(date: date, format: "d")}"/>
    <g:hiddenField name="${name}_month" value="${formatDate(date: date, format: "M")}"/>
    <g:hiddenField name="${name}_year" value="${formatDate(date: date, format: "yyyy")}"/>
    <g:if test="${showTime}">
        <g:hiddenField name="${name}_hour" value=""/>
    </g:if>
</span>
<g:javascript>
    $(function() {
           $.datepicker.setDefaults($.datepicker.regional['${language}']);

           $('#${name}_datepicker').openteleDatePicker({
                fieldName: '${name}',
                nullable: ${nullable},
                minDate: '${minDate}',
                maxDate: '${maxDate}',
                changeMonth: ${changeMonth},
                changeYear: ${changeYear},
                javascriptFormat: '${javascriptFormat}',
                date: '${dateAsText}',
                buttonImage: '${resource(dir: 'images', file: 'calendar.png')}',
                is12HourClock: ${is12HourClock}
           });
    });

    var updateHour = function() {
        var hours = parseInt($('#${name}_input_hour').val());
        if (!(hours > 0)) {
            return;
        }

        var amPm = $('#${name}_ampm').val();
        var $toSet = $('input[name="${name}_hour"]');

        $toSet.val(hours);
        if (amPm == 'PM' && hours < 12) {
            $toSet.val(hours + 12);
        }
        if (amPm == 'AM' && hours == 12) {
            $toSet.val(hours - 12);
        }
    };

    $( "#${name}_input_hour" ).change(function() {
        updateHour();
    });

    $( "#${name}_ampm" ).change(function() {
        updateHour();
    });
</g:javascript>
