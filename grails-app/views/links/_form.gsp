<%@ page import="grails.converters.JSON; org.opentele.server.model.LinksCategory" %>

<div class="content fieldcontain">
    <div class="input-group">
        <label for="name"
               class="property-label-small"
               data-tooltip="${message(code: 'linksCategory.name.tooltip')}">
            <g:message code="linksCategory.name.label" default="Category"/>
            <span class="required-indicator">*</span>
        </label>
        <g:textField name="name"
                     maxlength="255"
                     value="${category?.name}"
                     data-tooltip="${message(code: 'linksCategory.name.tooltip')}"/>
    </div>

    <div class="input-group">
        <label class="property-label-small"><g:message code="linksCategory.link.label"/></label>
        <table id="linksTable" class="form-table">
            <thead>
            <tr>
                <th><g:message code="linksCategory.link.title.label"/></th>
                <th><g:message code="linksCategory.link.url.label"/></th>
                <th class="link-action-cell"><g:message code="linksCategory.link.action.label"/></th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>

    <div class="input-group">
        <label for="newTitle" class="property-label-small" id="new-link-label"
               data-tooltip="${message(code: 'linksCategory.link.title.tooltip')}">
            <g:message code="linksCategory.link.add.label" default="Add"/>
        </label>
        <g:textField id="newTitle" name="newTitle" class="property-value-small"
                     placeholder="${message(code: 'linksCategory.link.title.placeholder')}"
                     size="10"
                     maxlength="128"
                     data-tooltip="${message(code: 'linksCategory.link.title.tooltip')}"/>
        <g:textField id="newUrl" name="newUrl" class="property-value-wide"
                     placeholder="${message(code: 'linksCategory.link.url.placeholder')}"
                     maxlength="255"
                     data-tooltip="${message(code: 'linksCategory.link.url.tooltip')}"/>
        <input type="button" id="addNewLink" class="button-add button-icon-only">
    </div>

    <div class="input-group">
        <label for="selected" class="property-label-small">
            <g:message code="linksCategory.patientgroup.label" default="Message:"/>
        </label>
        <g:select name="selected" from="${allPatientGroups}"
                  class="property-value-small"
                  optionKey="id" optionValue="${{ it?.name }}"
                  value="${currentPatientGroupIds}"
                  multiple="multiple"/>
    </div>
</div>

<g:javascript>

    $(document).ready(function() {
        $('input[name="links"]').val(JSON.stringify([]));
        window.links = <%=JSON.parse(currentLinks)%>;
        $("#linksTable > tbody").html("");
        links.forEach(function(link) {
            addLinkToTable(link.title, link.url)
         });
    });

    var shouldAddNewLink = function() {
        var title = $('#newTitle').val()
        var url = $('#newUrl').val()
        if ((title === '' || title === null) || (url === '' || url === null)) {
            return false;
        } else {
            return true;
        }
    };

    $('.save').click(function() {
        $('input[name="links"]').val(JSON.stringify(links));
    })

    $('#addNewLink').click(function() {
        if (!shouldAddNewLink()) {
            return;
        }

        var $title = $('#newTitle');
        var $url = $('#newUrl');
        if (isUniqueLink($title.val(), $url.val())) {
            addLinkToTable($title.val(), $url.val())
            links.push({title: $title.val(), url: $url.val()});
        }
        $title.val('')
        $url.val('')
        $title.focus();
    });

    var isUniqueLink = function(title, url) {
        var match = $.grep(links, function(element) {
            return element.title === title && element.url === url;
        });
        return match.length === 0;
    };

    var addLinkToTable = function(title, url) {
        var $table = $('#linksTable').find('tbody:last')
        var $row= $('<tr></tr>').appendTo($table)
        $('<td class="title"></td>').append(title).appendTo($row)
        $('<td class="url"></td>').append(url).appendTo($row)
        $('<td></td>').append('<g:img id="editLink" dir="images" file="edit.png" class="edit editLink"/>' + '<g:img
        id="deleteLink" dir="images" file="delete.png" class="delete deleteLink"/>')
                      .appendTo($row)
    };

    $(".deleteLink").live('click', function(event) {
        var $row = $(this).parent().parent();
        var title = $row.children('td.title').text();
        var url = $row.children('td.url').text();
        $row.remove();

        removeFromLinks(title, url);

    });

    $(".editLink").live('click', function(event) {
        var $title = $('#newTitle');
        var $url = $('#newUrl');

        var $row = $(this).parent().parent();
        $title.val($row.children('td.title').text());
        $url.val($row.children('td.url').text());
        $row.remove();
        removeFromLinks($title.val(), $url.val());
    });

    var removeFromLinks = function(title, url) {
        window.links = $.grep(links, function(element) {
            return element.title !== title || element.url !== url;
        });
    };

</g:javascript>