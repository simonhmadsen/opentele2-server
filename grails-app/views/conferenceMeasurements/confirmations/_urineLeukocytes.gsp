<td><g:message code="conferenceMeasurement.urineLeukocytes.confirmation.title"/></td>
<td>
    <span>
        <div>
            <g:message code="conferenceMeasurement.urineLeukocytes.confirmation.urineLeukocytes"
                       args="[measurement.leukocytesInUrine]"/>
        </div>
    </span>
</td>
