<td><g:message code="conferenceMeasurement.urineBlood.confirmation.title"/></td>
<td>
    <span>
        <div>
            <g:message code="conferenceMeasurement.urineBlood.confirmation.urineBlood"
                       args="[measurement.bloodInUrine]"/>
        </div>
    </span>
</td>
