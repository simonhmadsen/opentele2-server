<td><g:message code="conferenceMeasurement.urine.confirmation.title"/></td>
<td>
    <span>
        <div>
            <g:message code="conferenceMeasurement.urine.confirmation.protein"
                       args="[measurement.protein]"/>
        </div>
    </span>
</td>
