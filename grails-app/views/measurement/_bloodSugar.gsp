<g:if test="${bloodSugarData.empty}">
    <h1 class="information"><g:message code="patient.bloodSugarTable.noMeasurements"/></h1>
</g:if>
<g:else>
    <table id="bloodsugartable">
        <thead>
        <th><g:message code="patient.bloodSugarTable.date"/></th>
        <th><g:formatDate type="time" style="SHORT" date="${new java.text.SimpleDateFormat('HH.mm').parse("00.00")}" />-<g:formatDate type="time" style="SHORT" date="${new java.text.SimpleDateFormat('HH.mm').parse("04.59")}" /></th>
        <th><g:formatDate type="time" style="SHORT" date="${new java.text.SimpleDateFormat('HH.mm').parse("05.00")}" />-<g:formatDate type="time" style="SHORT" date="${new java.text.SimpleDateFormat('HH.mm').parse("10.59")}" /></th>
        <th><g:formatDate type="time" style="SHORT" date="${new java.text.SimpleDateFormat('HH.mm').parse("11.00")}" />-<g:formatDate type="time" style="SHORT" date="${new java.text.SimpleDateFormat('HH.mm').parse("16.59")}" /></th>
        <th><g:formatDate type="time" style="SHORT" date="${new java.text.SimpleDateFormat('HH.mm').parse("17.00")}" />-<g:formatDate type="time" style="SHORT" date="${new java.text.SimpleDateFormat('HH.mm').parse("23.59")}" /></th>
        </thead>
        <tbody>
        <g:each in="${bloodSugarData}" status="i" var="bloodsugarDataOnDate">
            <tr>
                <td>
                    <g:formatDate type="date" style="MEDIUM" date="${bloodsugarDataOnDate.date}" />
                </td>
                <g:render template="/measurement/bloodsugarMeasurements" bean="${bloodsugarDataOnDate.zeroToFour}" var="measurements"/>
                <g:render template="/measurement/bloodsugarMeasurements" bean="${bloodsugarDataOnDate.fiveToTen}" var="measurements"/>
                <g:render template="/measurement/bloodsugarMeasurements" bean="${bloodsugarDataOnDate.elevenToSixteen}" var="measurements"/>
                <g:render template="/measurement/bloodsugarMeasurements" bean="${bloodsugarDataOnDate.seventeenToTwentyThree}" var="measurements"/>
            </tr>
        </g:each>
        </tbody>
    </table>
    <div id="bloodsugardescription">
        <g:img file="beforeMeal.png" />
        <g:message code="patient.overview.bloodsugar.beforemeal"/>
        <br />
        <g:img file="afterMeal.png" />
        <g:message code="patient.overview.bloodsugar.aftermeal"/>
    </div>
</g:else>
