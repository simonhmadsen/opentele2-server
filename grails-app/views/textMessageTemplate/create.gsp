<%@ page import="org.opentele.server.model.TextMessageTemplate" %>
<!doctype html>
<html>
<head>
    <meta name="layout" content="main">
    <title><g:message code="default.create.label" args="[g.message(code:'textMessageTemplate.label')]" /></title>
</head>
<body>
<div id="create-textMessageTemplate" class="content scaffold-create" role="main">
    <h1><g:message code="default.create.label" args="[g.message(code:'textMessageTemplate.label')]" /></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <g:hasErrors bean="${template}">
        <ul class="errors" role="alert">
            <g:eachError bean="${template}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>
    <g:form action="save" >
        <fieldset class="form">
            <g:render template="form"/>
        </fieldset>
        <fieldset class="buttons">
            <g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" />
        </fieldset>
    </g:form>
</div>
</body>
</html>
