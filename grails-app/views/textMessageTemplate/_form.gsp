<%@ page import="org.opentele.server.model.TextMessageTemplate" %>

<div class="fieldcontain ${hasErrors(bean: template, field: 'name', 'error')}">
    <label for="name"
           data-tooltip="${message(code: 'textMessageTemplate.name.tooltip')}">
        <g:message code="textMessageTemplate.name.label" default="Name" />
        <span class="required-indicator">*</span>
    </label>
    <g:textField name="name"
                 maxlength="255"
                 value="${template?.name}"
                 data-tooltip="${message(code: 'textMessageTemplate.name.tooltip')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: template, field: 'content', 'error')} ">
    <label for="content"
           data-tooltip="${message(code: 'textMessageTemplate.content.tooltip')}">
        <g:message code="textMessageTemplate.content.label"
                   default="Message Text" />
        <span class="required-indicator">*</span>
    </label>
    <g:textArea name="content"
                cols="40"
                rows="5"
                maxlength="1024"
                value="${template?.content}"
                data-tooltip="${message(code: 'textMessageTemplate.content.tooltip')}"/>
</div>
