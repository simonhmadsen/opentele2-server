package org.opentele.server.provider.service

import grails.gsp.PageRenderer
import grails.plugin.mail.MailService
import grails.transaction.NotTransactional

class MailSenderService {

    def grailsApplication
    MailService mailService
    PageRenderer groovyPageRenderer

    boolean getMailEnabled() {
        def mailDisabled = Boolean.valueOf(grailsApplication.config.grails.mail.disabled)
        !mailDisabled && grailsApplication.config.grails.mail.host
    }

    @NotTransactional
    boolean sendMail(String mailSubject, String mailTo, String template, Map model) {
        def mailHtml = renderTemplate(template, model)
        mailService.sendMail {
            to(mailTo)
            subject(mailSubject)
            html(mailHtml)
        }
        return true
    }

    private String renderTemplate(String template, Map model) {
        groovyPageRenderer.render (template: template, model: model)

    }
}
