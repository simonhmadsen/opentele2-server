package org.opentele.server.provider.sosi

import dk.sosi.seal.SOSIFactory
import dk.sosi.seal.model.IDCard
import dk.sosi.seal.model.Request
import org.codehaus.groovy.grails.commons.GrailsApplication
import org.opentele.server.model.Clinician
import org.opentele.server.provider.util.SosiUtil

class SosiService {

    // --*-- Fields --*--

    SosiUtil sosiUtil
    String cvr
    String systemName

    // --*-- Methods --*--

    //TODO mss check that this method is called
    void setGrailsApplication(GrailsApplication grailsApplication) {
        cvr = grailsApplication.config.seal.cvr
        systemName = grailsApplication.config.seal.systemName
        sosiUtil = new SosiUtil(grailsApplication.config)
    }

    def createRequest() {
        Request sosiRequest = createSosiRequest()
        setSignedIDCard(sosiRequest)
        sosiRequest
    }

    def createUnsignedRequest(String username, String password,
                              String alternativeIdentifier) {
        Request sosiRequest = createSosiRequest()
        setUnsignedSystemIDCard(sosiRequest, username, password, alternativeIdentifier)
        sosiRequest
    }

    private Request createSosiRequest() {
        SOSIFactory sosiFactory = sosiUtil.getSOSIFactory()
        sosiFactory.createNewRequest(false, "flow")
    }

    private void setSignedIDCard(Request sosiRequest) {
        IDCard idCard = sosiUtil.createSignedIDCard()
        sosiRequest.setIDCard(idCard)
    }

    private void setUnsignedSystemIDCard(Request sosiRequest,
                                         String username, String password,
                                         String alternativeIdentifier) {
        IDCard idCard = sosiUtil.createUnsignedSystemIDCard(username, password,
                alternativeIdentifier)
        sosiRequest.setIDCard(idCard)
    }

}
