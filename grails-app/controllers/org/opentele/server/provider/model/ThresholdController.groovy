package org.opentele.server.provider.model
import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import org.opentele.server.core.model.types.PermissionName
import org.opentele.server.model.Threshold
import org.opentele.server.provider.StandardThresholdSetService
import org.opentele.server.provider.ThresholdService

@Secured(PermissionName.NONE)
class ThresholdController  {
    ThresholdService thresholdService
    StandardThresholdSetService standardThresholdSetService
    static allowedMethods = [update: "POST"]

    @Secured(PermissionName.THRESHOLD_WRITE)
    def edit(Long id) {
        session.setAttribute('lastReferer', request.getHeader('referer'))

        def threshold = Threshold.get(id)
        if (!threshold) {
            return notFound(id)
        }

        def command = thresholdService.getThresholdCommandForEdit(threshold)
        def standardThresholdSet = standardThresholdSetService.findStandardThresholdSetForThreshold(threshold)

        render(view: '/threshold/edit', model: [
                command: command,
                patientGroup: standardThresholdSet?.patientGroup
        ])
    }

    @Secured(PermissionName.THRESHOLD_WRITE)
    def update(Long id) {
        def threshold = Threshold.get(id)
        if(!threshold) {
            return notFound(params.id)
        }

        def command = thresholdService.getThresholdCommandForEdit(threshold)
        bindData(command, params)

        try {
            thresholdService.updateThreshold(command)

            flash.message = message(code: 'default.updated.message', args: [message(code: 'standardThreshold.label', default: 'StandardThreshold'), threshold.id])
            def lastReferer = session.getAttribute('lastReferer')
            if (lastReferer) {
                session.removeAttribute('lastReferer')
                redirect(url: lastReferer)
            }
        } catch (ValidationException ex) {
            command.errors = ex.errors
            def standardThresholdSet = standardThresholdSetService.findStandardThresholdSetForThreshold(threshold)
            render(view: '/threshold/edit', model: [
                command: command,
                patientGroup: standardThresholdSet?.patientGroup
            ])
        }
    }

    private notFound(def id) {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'standardThreshold.label', default: 'StandardThreshold'), id])
        redirect(action: session.lastAction, controller: session.lastController)
    }


}
