package org.opentele.server.provider

import grails.converters.JSON
import grails.plugin.springsecurity.annotation.Secured
import org.codehaus.groovy.grails.web.json.JSONArray
import org.opentele.server.core.model.types.PermissionName
import org.opentele.server.model.LinksCategory
import org.opentele.server.model.PatientGroup

class LinksController {

    def linksService

    @Secured(PermissionName.LINKS_CATEGORIES_READ_ALL)
    def index() {

        redirect(action: "list", params: params)
    }

    @Secured(PermissionName.LINKS_CATEGORIES_READ_ALL)
    def list() {

        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        def categories = LinksCategory.list(params)
        [
                categories     : categories.collect {
                    [name: it.name, patientGroupNames: it.patientGroups*.name.join(', '), id: it.id]
                },
                categoriesTotal: LinksCategory.count()
        ]
    }

    @Secured(PermissionName.LINKS_CATEGORIES_READ)
    def show() {

        def category = LinksCategory.get(params.long('id'))
        if (!category) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'linksCategory.name.label', default: 'Category')])
            redirect(action: 'list')
            return
        }

        [category: category]
    }

    @Secured(PermissionName.LINKS_CATEGORIES_CREATE)
    def create() {

        return buildViewModel(new LinksCategory(links: [], patientGroups: []))
    }

    @Secured(PermissionName.LINKS_CATEGORIES_CREATE)
    def save() {

        def links = parseLinks()
        def patientGroupIds = parsePatientGroupIds()

        def created = linksService.createCategory(params.name.toString(), links, patientGroupIds)
        if (created.hasErrors()) {
            def viewModel = buildViewModel(created)
            render(view: "create", model: viewModel)
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'linksCategory.name.label', default: 'Category')])
        redirect(action: 'list')
    }

    @Secured(PermissionName.LINKS_CATEGORIES_WRITE)
    def edit() {

        def category = LinksCategory.get(params.long('id'))
        if (!category) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'linksCategory.name.label', default: 'Category')])
            redirect(action: 'list')
            return
        }

        return buildViewModel(category)
    }

    @Secured(PermissionName.LINKS_CATEGORIES_WRITE)
    def update() {

        def links = parseLinks()
        def patientGroupIds = parsePatientGroupIds()

        try {
            def updated = linksService.updateCategory(params.long('id'), params.long('version'), params.name.toString(), links, patientGroupIds)
            if (updated.hasErrors()) {
                def viewModel = buildViewModel(updated)
                render(view: 'edit', model: viewModel)
                return
            }

            flash.message = message(code: 'default.updated.message', args: [message(code: 'linksCategory.name.label', default: 'Category')])
            redirect(action: "show", id: updated.id)
        } catch (EntityNotFoundException) {
            flash.message = message(code: 'linksCategory.deleted.by.other')
            redirect(action: "list")
        }
    }

    @Secured(PermissionName.LINKS_CATEGORIES_DELETE)
    def delete() {

        def messageCode = 'default.deleted.message'
        try {
            linksService.deleteCategory(params.long("id"))
        } catch (EntityNotFoundException) {
            messageCode = 'default.not.found.message'
        }

        flash.message = message(code: messageCode, args: [message(code: 'linksCategory.name.label', default: 'Category')])
        redirect(action: "list")
    }

    private def buildViewModel(LinksCategory category) {

        String links = category.links != null ? category.links as JSON : [] as JSON
        def patientGroups = category.patientGroups != null ? category.patientGroups : []
        [
                category              : category,
                currentLinks          : links,
                allPatientGroups      : PatientGroup.list(),
                currentPatientGroupIds: patientGroups.collect { it.id }
        ]
    }

    private parseLinks() {

        def links = []
        if (params.links) {
            def linksAsJson = new JSONArray(params.links)
            links = linksAsJson.collect {
                [title: it.title, url: it.url]
            }
        }

        links
    }

    private parsePatientGroupIds() {
        def patientGroupIds = []
        if (params.selected) {
            patientGroupIds = params.selected.getClass() == String ? [params.long('selected')] : params.selected.collect {
                it.toLong()
            }
        }
        patientGroupIds
    }
}